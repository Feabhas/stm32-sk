#include <stdio.h>
#include <stdint.h>

typedef struct {
	uint32_t crl;	// config reg low
	uint32_t crh;	// config reg high
	uint32_t idr;	// i/p data reg
	uint32_t odr;	// o/p data reg
	uint32_t bsrr;	// bit set/reset reg
	uint32_t brr;	// bit reset reg
	uint32_t lckr;	// config lock register
} GPIO_type;

volatile GPIO_type* portA = ((GPIO_type*) 0x40010800);

#define APB2ClockEnableReg (*((volatile uint32_t*)0x40021018))

volatile int i = 0;

void wait(void) {
	for (int x = 0; x < 1000000; ++x) {
		++i;
	}
}

int main(void) {
	puts("Standard output message.");
	fprintf(stderr, "Standard error message.\n");

	// Enable GPIO Peripheral clock
	APB2ClockEnableReg |= (1 << 2);	// IOPAEN bit : IO port A clock enabled

	// set PortA bits 4 & 5 to output, general push-pull, 50Mhz
	// each bit configuration uses 4 bits
	// [0:1] define input or output: 11 defines output mode max speed at 50MHz
	// [2:3] if output define mode:  00 defines general purpose output push-pull
	portA->crl = ((0x03 << 16) | (0x03 << 20));

	// Infinite loop
	 do {
		portA->bsrr = (1 << 4);		// bit 4 set
		portA->brr  = (1 << 5);		// bit 5 reset
		wait();
		portA->brr  = (1 << 4);		// bit 4 reset
		portA->bsrr = (1 << 5);		// bit 5 set
		wait();
	} while (1);
}

// ----------------------------------------------------------------------------
